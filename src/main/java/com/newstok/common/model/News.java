package com.newstok.common.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Scope("prototype")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class News {
	private String id;
	private String type;
	private String title;
	private String subtitle;
	private String description;
	private boolean hasMedia;
	private String mediaLink;
	private String areaCode;
	private String city;
	private String created;
	private String userId;
}
