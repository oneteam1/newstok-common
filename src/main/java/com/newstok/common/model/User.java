package com.newstok.common.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Scope("prototype")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
	private String id;
	private String name;
	private String email;
	private String avatar;
}
